# gopherhole 🐹🕳

Gopher server for static files. Supports plaintext Gopher as well as Gopher over TLS on the same port. Does not transform files in any way.

## CLI

```shell
gopherhole
  --plain-root ./public
  --secure-root ./public
  --port 70
  --index gophermap
  --ca ca.pem
  --key key.pem
  --cert cert.pem
```

## API

```js
const { GopherHole } = require('gopherhole')
const { readFileSync } = require('fs')

const server = new GopherHole({
  // Directory served to Gopher over TCP clients.
  plaintextRoot: '/path/to/unencrypted/content',

  // Directory served to Gopher over TLS clients.
  // Template variable $domain contains the TLS SNI server name.
  // This can be used to serve multiple domains from one server.
  secureRoot: '/path/to/encrypted/content/$domain',

  // Filename to serve when client requests a directory.
  index: 'gophermap',

  // See: tls.createSecureContext()
  // https://nodejs.org/api/tls.html#tls_tls_createsecurecontext_options
  // Tip: Use `SNICallback` for dynamic loading of certificates
  //      based on the TLS SNI server name.
  key: readFileSync('./key.pem'),
  cert: readFileSync('./cert.pem'),
  ca: [readFileSync('./ca.pem')]
})

server.listen({ port: 70 }, () => {
  const { port } = server.address()
  console.log(`Listening on gopher://localhost:${port}`)
})
```
