const { GopherServer } = require('goth')
const { resolve, join } = require('path')
const { createReadStream } = require('fs')
const { once } = require('events')

class Gopherhole extends GopherServer {
  constructor (options) {
    super(options)
    const {
      plaintextRoot = process.cwd(),
      secureRoot = process.cwd(),
      index = 'gophermap'
    } = options
    this.on('connection', (socket) => {
      socket.setTimeout(10000, () => socket.end())
    })
    this.on('gopherConnection', async (socket, type) => {
      socket.on('error', ({ code, message }) => {
        if (code !== 'EPIPE') {
          console.error(message)
        }
      })
      socket.once('data', async (chunk) => {
        const selector = resolve('/', chunk.toString().split(/[\t\r\n]/, 1)[0])
        console.log('Request:', selector)
        const root = (type === 'tls' && socket.servername)
          ? secureRoot.replace(
            '$domain',
            socket.servername
              .substr(0, 255)
              .replace('..', '')
              .replace(/[A-Za-z0-9-.]/, '')
          )
          : plaintextRoot
        const filepath = resolve(process.cwd(), join(root, selector))
        try {
          await serveFile(socket, filepath)
        } catch (error) {
          if (error.code === 'EISDIR') {
            try {
              await serveFile(socket, join(filepath, index))
            } catch (error) {
              socket.end('3File not found\t\terror.host\t1\r\n.\r\n')
            }
          } else if (error.code === 'ENOENT') {
            socket.end('3File not found\t\terror.host\t1\r\n.\r\n')
          } else {
            socket.end()
          }
        }
      })
    })
  }
}

async function serveFile (socket, filepath) {
  const file = createReadStream(filepath)
  await once(file, 'open')
  file.pipe(socket)
  await once(file, 'close')
  socket.end()
}

module.exports.Gopherhole = Gopherhole
