const test = require('blue-tape')
const { join } = require('path')
const { fork } = require('child_process')
const net = require('net')
const { once } = require('events')

const modulePath = join(__dirname, '../cli.js')

test('Run the CLI', async (t) => {
  const port = 7070

  const args = [
    '--port', port,
    '--plaintextRoot', join(__dirname, 'fixtures/plaintext')
  ]
  const options = {
    stdio: ['inherit', 'pipe', 'pipe', 'ipc']
  }
  const child = fork(modulePath, args, options)
  child.stderr.on('data', (chunk) => {
    process.stderr.write('CLI STDERR: ' + chunk.toString())
  })
  child.stdout.on('data', (chunk) => {
    process.stdout.write('CLI STDOUT: ' + chunk.toString())
  })
  const [ready] = await once(child.stdout, 'data')
  t.is(ready.toString(), `Listening on gopher://localhost:${port}\n`)

  const client = net.createConnection({ port })
  await once(client, 'connect')
  client.write('/\r\n')
  const [received] = await once(client, 'data')
  t.is(received.toString(), 'Hello, Plaintext World!')
  client.end()

  child.kill()
  const [code] = await once(child, 'exit')
  t.is(code, null)
})

test('Server throws error', async (t) => {
  const options = {
    stdio: 'inherit'
  }
  const args = [
    '--port', -1
  ]
  const child = fork(modulePath, args, options)
  const [code] = await once(child, 'exit')
  t.is(code, 1)
})
