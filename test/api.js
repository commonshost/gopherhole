const test = require('blue-tape')
const { Gopherhole } = require('..')
const net = require('net')
const tls = require('tls')
const { once } = require('events')
const { join } = require('path')
const { readFileSync } = require('fs')

test('Plaintext client connection', async (t) => {
  const server = new Gopherhole({
    plaintextRoot: join(__dirname, 'fixtures/plaintext')
  })
  server.listen()
  await once(server, 'listening')

  const client = net.createConnection(server.address())
  await once(client, 'connect')
  client.write('/\r\n')
  const [received] = await once(client, 'data')
  t.is(received.toString(), 'Hello, Plaintext World!')
  client.end()

  server.close()
  await once(server, 'close')
})

test('TLS client connection', async (t) => {
  const server = new Gopherhole({
    secureRoot: join(__dirname, 'fixtures/secure'),
    ecdhCurve: 'P-384:P-256',
    key: readFileSync('key.pem'),
    cert: readFileSync('cert.pem'),
    ca: [/* readFileSync('ca.pem') */]
  })
  server.listen()
  await once(server, 'listening')

  const client = tls.connect({
    port: server.address().port,
    servername: 'example.net',
    ALPNProtocols: ['gopher'],
    rejectUnauthorized: false
  })
  await once(client, 'connect')
  client.write('/\r\n')
  const [received] = await once(client, 'data')
  t.is(received.toString(), 'Hello, Secure World!')
  client.end()

  server.close()
  await once(server, 'close')
})
