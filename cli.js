#!/usr/bin/env node

const yargs = require('yargs')
const { Gopherhole } = require('.')
const { readFileSync } = require('fs')

const { argv } = yargs
  .env('GOPHERHOLE')
  .option('port', {
    type: 'number',
    describe: 'Port for incoming connections',
    default: 70
  })
  .option('plaintext-root', {
    type: 'string',
    describe: 'Directory served to Gopher over TCP clients',
    default: '.'
  })
  .option('secure-root', {
    type: 'string',
    describe: 'Directory served to Gopher over TLS clients',
    default: '.'
  })
  .option('index', {
    type: 'string',
    describe: 'Filename to serve when client requests a directory',
    default: 'gophermap'
  })
  .option('public-certificate', {
    alias: 'cert',
    type: 'string',
    describe: 'File path of the TLS public certficate',
    default: ''
  })
  .option('private-key', {
    alias: 'key',
    type: 'string',
    describe: 'File path of the TLS private key',
    default: ''
  })
  .option('certificate-authority', {
    alias: 'ca',
    type: 'array',
    describe: 'File paths of the CA certificate chain',
    default: []
  })
  .usage('')
  .usage('Example: $0 --key key.pem --cert cert.pem --ca ca.pem --port 70 --')
  .config()
  .version()
  .help()
  .wrap(null)

const options = {
  port: argv.port,
  plaintextRoot: argv.plaintextRoot,
  secureRoot: argv.secureRoot,
  index: argv.index,
  cert: argv.publicCertificate
    ? readFileSync(argv.publicCertificate)
    : undefined,
  key: argv.privateKey
    ? readFileSync(argv.privateKey)
    : undefined,
  ca: argv.certificateAuthority.length > 0
    ? argv.certificateAuthority.map((ca) => readFileSync(ca))
    : undefined
}

const server = new Gopherhole(options)

server.on('error', ({ message }) => {
  console.error(message)
  process.exit(1)
})

try {
  server.listen({ port: options.port }, () => {
    const { port } = server.address()
    console.log(`Listening on gopher://localhost:${port}`)
  })
} catch (error) {
  server.emit('error', error)
}
